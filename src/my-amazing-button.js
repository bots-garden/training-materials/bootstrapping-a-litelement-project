import { LitElement, html, property } from 'lit-element';

import {style} from './main-styles.js';

export class MyAmazingButton extends LitElement {

  
  static get properties() {
    return {
      counter: { type: Number }
    }
  }
  
  //@property({ type: Number })
  //counter = 0

  constructor() {
    super()
    this.counter = 0
  }
  render(){
    return html`
      ${style}
      <div>
        <button @click="${this.clickHandler}" class="button">👋 Click me! ${this.counter}</button>
      </div> 
    `
  }

  clickHandler() {
    this.counter+=1

    this.dispatchEvent(
      new CustomEvent('add-buddy', {  
        bubbles: true, composed: true, detail: {
          value: `🐻 Teddy n°${this.counter}`
        } 
      })
    )
    
  }
}
customElements.define('my-amazing-button', MyAmazingButton)